#include <iostream>
#include <thread>
#include "socket.h"


class TAcceptHandler {
    private:
        TSocket Socket;

    public:
        bool HandleAcceptedSocket(TSocket sock) {
            Socket = sock;
            return true;
        }
        const TSocket &GetSocket() const {
            return Socket;
        }
};


TSocket CreateConnection(int port) {
    TSocket s;
    s.Bind(port, "");
    TAcceptHandler handler;
    s.AcceptLoop(handler);
    return handler.GetSocket();
}

class TDataHandler {
    public:
        bool ProcessReceivedData(const char *data, size_t sz) const {
            for (; sz > 0; --sz, ++data)
                std::cout << *data;
            std::cout << std::flush;
            return false;
        }
};

int main() {
    TSocket s = CreateConnection(13232);
    TDataHandler handler;
    std::thread t([&s, &handler] () {
        s.RecvLoop(handler);
    });
    t.join();
    return 0;
}

